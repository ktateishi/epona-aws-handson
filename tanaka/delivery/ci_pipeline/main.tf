provider "aws" {
  assume_role {
    # TODO: role_arnのロール名 [LastName]を修正
    role_arn = "arn:aws:iam::938285887320:role/[LastName]TerraformExecutionRole"
  }
}

# Amazon Linux 2 の最新版を取得する
data "aws_ami" "recent_amazon_linux_2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"]
  }

  filter {
    name   = "state"
    values = ["available"]
  }
}

module "ci_pipeline" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/ci_pipeline?ref=v0.2.1"

  # TODO: name, tags.Ownerの[last_name]を修正
  name = "[last_name]-gitlab-runner"
  tags = {
    Owner       = "[last_name]"
    Environment = "delivery"
    ManagedBy   = "epona"
  }

  vpc_id = data.terraform_remote_state.delivery_network.outputs.network.vpc_id

  # TODO: bucket_nameの先頭[last_name]を修正
  static_resource_buckets = [{
    bucket_name        = "[last_name]-static-resource"
    force_destroy      = true # PJ適用時にS3バケットの削除保護の要否に応じて設定してください
    runtime_account_id = "922032444791"
  }]

  container_image_repositories = [{
    # TODO: nameの先頭[last_name]を修正
    name                 = "[last_name]-chat-example-backend"
    image_tag_mutability = "MUTABLE"

    repository_policy = data.aws_iam_policy_document.cross_account_pull_policy.json
    lifecycle_policy  = file("${path.module}/lifecycle_policy.json")
    }, {
    # TODO: nameの先頭[last_name]を修正
    name                 = "[last_name]-chat-example-ntfr"
    image_tag_mutability = "MUTABLE"

    repository_policy = data.aws_iam_policy_document.cross_account_pull_policy.json
    lifecycle_policy  = file("${path.module}/lifecycle_policy.json")
  }]

  ci_runner_ami                    = data.aws_ami.recent_amazon_linux_2.image_id
  ci_runner_instance_type          = "t3.small"
  ci_runner_root_block_volume_size = "30"
  ci_runner_subnet                 = data.terraform_remote_state.delivery_network.outputs.network.private_subnets[0]

  gitlab_runner_registration_token = var.gitlab_runner_registration_token
}

# Runtime環境からクロスアカウントでPull Accessを可能にするポリシー
# see: https://stackoverflow.com/questions/52914713/aws-ecs-fargate-pull-image-from-a-cross-account-ecr-repo/52934781
data "aws_iam_policy_document" "cross_account_pull_policy" {
  statement {
    actions = [
      "ecr:GetDownloadUrlForLayer",
      "ecr:BatchCheckLayerAvailability",
      "ecr:BatchGetImage"
    ]
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::922032444791:root"]
    }
  }
}
