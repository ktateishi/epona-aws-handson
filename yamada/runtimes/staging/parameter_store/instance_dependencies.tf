data "terraform_remote_state" "staging_encryption_key" {
  backend = "s3"

  config = {
    bucket         = "yamada-staging-terraform-tfstate"
    key            = "encryption_key/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "yamada_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::938285887320:role/YamadaStagingTerraformBackendAccessRole"
  }
}
