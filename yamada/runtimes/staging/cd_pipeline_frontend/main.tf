provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::922032444791:role/YamadaTerraformExecutionRole"
  }
}

module "cd_pipeline_frontend" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/cd_pipeline_frontend?ref=v0.2.1"

  tags = {
    Owner              = "yamada"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }
  delivery_account_id = "938285887320"

  pipeline_name = "yamada-pipeline-frnt"

  artifact_store_bucket_name          = "yamada-frnt-artifacts"
  artifact_store_bucket_force_destroy = true # PJ適用時にS3バケットの削除保護の要否に応じて設定してください

  # Delivery環境のS3にCodePipelineからクロスアカウントでアクセスするためのロール
  # cd_pipeline_frontend_trigger の output として出力される
  cross_account_codepipeline_access_role_arn = "arn:aws:iam::938285887320:role/Yamada-Pipeline-FrntAccessRole"

  # ci_pipeline の bucket_name と同じ名前を指定する
  source_bucket_name = "yamada-static-resource"
  source_object_key  = "source.zip"

  # cachable_frontend s3_frontend_bucket_name と同じ名前を指定する
  deployment_bucket_name = "yamada-chat-example-frontend"
  deployment_object_path = "public"

  deployment_require_approval = false

  cache_invalidation_config = {
    enable                     = true
    cloudfront_distribution_id = data.terraform_remote_state.staging_cacheable_frontend.outputs.cacheable_frontend.cloudfront_id
  }
}
